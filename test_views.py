import json
import requests
import pytest
from unittest.mock import patch

from app.models.letter import letter_sync, Letter, Timeline
from app import db


def test_ping(client):
    assert client.get('/ping').status_code == 200


def test_letter_status_404(client):
    assert client.get('/letters/123AOE').status_code == 404


MOCK_RESPONSE = {
    'shipment': {
        'timeline': [
            {
                'country': '',
                'id': 1,
                'longLabel': '',
                'shortLabel': 'Courrier en préparation chez '
                              "l'expéditeur",
                'status': True,
                'type': 1
            },
            {
                'country': '',
                'id': 2,
                'longLabel': '',
                'shortLabel': "En cours d'acheminement",
                'status': True,
                'type': 1
            },
            {
                'country': '',
                'id': 3,
                'longLabel': '',
                'shortLabel': 'Arrivé sur le site de distribution',
                'status': True,
                'type': 1,
            },
            {
                'country': '',
                'date': '2020-03-14T00:00:00+01:00',
                'id': 5,
                'longLabel': 'Votre courrier est déposé dans la '
                             'boîte à lettres du destinataire.',
                'shortLabel': 'Courrier distribué',
                'status': True,
                'type': 1
            }
        ],
        'url': 'https://www.laposte.fr/outils/suivre-vos-envois?code=3P11111111110'
    }
}


@pytest.fixture(autouse=True)
def db_clean():
    db.session.query(Timeline).delete()
    db.session.query(Letter).delete()
    yield


def test_letter_status(client):
    letter = Letter(tracking_number='3C00638718711')
    db.session.add(letter)
    db.session.commit()
    with patch('app.models.letter.laposte_suivi') as mock:
        mock.return_value.status_code = 200
        mock.return_value.json.return_value = MOCK_RESPONSE
        response = client.get(f'/letters/{letter.id}')
        assert response.get_data().decode('utf8') == 'Courrier distribué'

        # test no update to do
        response = client.get(f'/letters/{letter.id}')
        assert response.get_data().decode('utf8') == 'Courrier distribué'

    with patch('app.models.letter.laposte_suivi') as mock:
        # test json error doesn't break it
        mock.return_value.json.side_effect = json.JSONDecodeError(
            'Failed', 'Unknown', 123)
        response = client.get(f'/letters/{letter.id}')
        assert response.get_data().decode('utf8') == 'Courrier distribué'

    with patch('app.models.letter.laposte_suivi') as mock:
        # test connection error doesn't break it
        mock.return_value.side_effect = requests.RequestException()
        response = client.get(f'/letters/{letter.id}')
        assert response.get_data().decode('utf8') == 'Courrier distribué'

    with patch('app.models.letter.laposte_suivi') as mock:
        # test 400 error doesn't break it
        mock.return_value.status_code == 400
        response = client.get(f'/letters/{letter.id}')
        assert response.get_data().decode('utf8') == 'Courrier distribué'
