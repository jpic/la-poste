from app.models.letter import Letter, Timeline
from app import db


def sandbox_data():
    """
    Insert sandbox data.

    For local use only, tests should not rely on network APIs in general
    because when these are not down they are at least a bottleneck.
    """
    FIXTURES = (
        ['4P36275770836', 'Courrier', 1, 200],
        ['1K36275770836', 'Courrier', 2, 200],
        ['4G11111111110', 'Courrier', 3, 200],
        ['RK633119313NZ', 'Courrier', 4, 200],
        ['3P11111111110', 'Courrier', 5, 200],
        ['XX032057485XX', 'Courrier', False, 404],
        ['8R11111111110', 'Colis', 0, 200],
        ['115111111111111', 'Colis', 1, 200],
        ['5S11111111110', 'Colis', 2, 200],
        ['3SAAAA1111111', 'Colis', 3, 200],
        ['6P01007508742', 'Colis', 4, 200],
        ['6T11111111110', 'Colis', 5, 200],
        ['6W111111111XX', 'Colis', False, 404],
        ['FD633119313NZ', 'Chronopost', 1, 200],
        ['GF111111110NZ', 'Chronopost', 2, 200],
        ['114111111111111', 'Chronopost', 3, 200],
        ['GM111111110NZ', 'Chronopost', 4, 200],
        ['ZZ111111110NZ', 'Chronopost', 5, 200],
        ['FA633119313XX', 'Chronopost', False, 404],
        ['6C12345678910', 'Colis', False, 400],
        ['6C22345678999', 'Colis', False, 401],
    )

    for fixture in FIXTURES:
        letter = db.session.query(
            Letter
        ).filter_by(
            tracking_number='1A00915820380'
        ).first()

        if not letter:
            letter = Letter(tracking_number=fixture[0])
            db.session.add(letter)
            db.session.commit()
            fixture.append(letter.id)
    return FIXTURES
