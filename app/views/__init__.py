from app import app, db
from app.models.letter import letter_sync_all_task, letter_sync, Letter


@app.route("/ping", methods=["GET"])
def ep_ping():
    return "pong", 200


@app.route("/letters", methods=["POST"])
def ep_setup_create_letter():
    letter = Letter()
    db.session.add(letter)
    db.session.commit()
    return f"All done: letter object {letter.id} has been created", 200


@app.route("/letters/sync/", methods=["GET"])
def ep_letter_sync():
    letter_sync_all_task.delay()
    return "Synchronizing all letters", 200


@app.route("/letters/<int:letter_id>", methods=["GET"])
def ep_letter_status(letter_id):
    letter = letter_sync(letter_id, db.session)
    db.session.commit()

    if not letter:
        return "Letter not found", 404

    return letter.status or "No status yet", 200
