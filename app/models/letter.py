from datetime import datetime
import json
import requests
from app import celery, db


class Letter(db.Model):
    __tablename__ = "letter"

    id = db.Column(db.Integer, primary_key=True)
    tracking_number = db.Column(db.String(256))
    status = db.Column(db.String(191))


class Timeline(db.Model):
    __tablename__ = 'timeline'
    id = db.Column(db.Integer, primary_key=True)
    upstream_id = db.Column(db.Integer)
    letter = db.Column(db.Integer, db.ForeignKey('letter.id'))
    date_time = db.Column(db.DateTime)
    short_label = db.Column(db.String(191))


def laposte_suivi(letter_id):
    return requests.get(
        f'https://api.laposte.fr/suivi/v2/idships/{letter_id}',
        headers={
            'Accept': 'application/json',
            'X-Okapi-Key': 'qxTw5Fvw3D+CudODfQFPe+PvN0oL5163omDnBcb20o/dPj26qCUT/lsJnFzb/o0J',
        }
    )


@celery.task
def letter_sync_all_task():
    for letter in db.session.query(Letter):
        letter_sync(letter.id, db.session)
    db.session.commit()


def letter_sync(letter_id, session):
    letter = session.query(
        Letter
    ).filter_by(
        id=letter_id
    ).first()

    if not letter:
        return None

    try:
        api = laposte_suivi(letter.tracking_number)
    except requests.RequestException:
        print("logger.error: Request error! Returning cached")
        return letter

    if api.status_code != 200:
        print("logger.error: Non 200 response! Returning cached")
        return letter

    try:
        data = api.json()
    except json.JSONDecodeError:
        print("logger.error: Invalid JSON! Returning cached")
        return letter

    timelines = {
        timeline.upstream_id: timeline
        for timeline in session.query(Timeline).filter_by(
            letter=letter_id,
        )
    }

    shipment = data.get('shipment', {})
    for timeline_data in shipment.get('timeline', []):
        timeline_id = timeline_data.get('id', None)
        if not timeline_id:
            print(f'logger: No id in {timeline_data}')
            continue

        timeline_short_label = timeline_data.get('shortLabel', None)
        if not timeline_short_label:
            print(f'logger: No shortLabel in {timeline_data}')
            continue

        timeline_date = timeline_data.get('date', None)
        timeline_date_time = None
        if timeline_date:
            try:
                timeline_date_time = datetime.fromisoformat(timeline_date)
            except ValueError:
                pass  # malformed datetime in the "date" field

        if timeline_id in timelines:
            timeline = timelines[timeline_id]
            if (
                timeline.short_label == timeline_short_label
                and timeline.date_time == timeline_date_time
            ):
                continue  # already up to date
        else:
            timeline = Timeline(
                letter=letter.id,
                upstream_id=timeline_id,
            )
        timeline.short_label = timeline_short_label
        timeline.date_time = timeline_date_time
        session.add(timeline)
        timelines[timeline.upstream_id] = timeline

    if not timelines:
        return 'Status unknown yet', 200

    letter.status = timelines[max(timelines.keys())].short_label
    session.add(letter)
    return letter
