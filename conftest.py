import pytest

from app import app as flask_app
from app import db as flask_db


@pytest.fixture
def app():
    return flask_app
